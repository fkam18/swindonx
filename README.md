First Build (4 May 2016)
- raspbian based

Target Features (4 Feb 2016)
- hierarchical name space for a flock and flocks of IoT's 
- distributed whiteboard
- p2p messaging
- reverse client-server / no central node
- asynchronous on/offline support
- sensor aware
- self cloning/replication
- system is data
- self stabilization in the broad sense
- self destruct
- survive unstable power environment
- self repair
- organic security 
###
